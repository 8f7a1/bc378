const { MongoClient } = require('mongodb')

let client

function connectDb() {
    client = new MongoClient("mongodb://127.0.0.1:27017")
    // const db = client.db("dlms")
    // const meter = db.collection("meter")
}

function getDbClient() {
    return client
}

module.exports = {
    connectDb,
    getDbClient
}