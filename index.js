const express = require('express')
const bodyParser = require('body-parser')
const getSomething = require('./controller/getSomething')
const createSomething = require('./controller/createSomething')
const meterController = require('./controller/meter')
const { connectDb } = require("./db")

connectDb()

const app = express()
app.use(bodyParser.json())
app.get('/query/something', getSomething)
app.post('/create/something', createSomething)

app.get('/meters', meterController.getMeters)
app.post('/meter', meterController.createMeter)
app.post('/meters', meterController.createMeters)

app.listen(3000, function () {
    console.log("server ready at port 3000")
})