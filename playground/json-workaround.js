var json_str = "{\"name\":\"Chanon\", \"lastName\":\"Srisook\"}"
console.log("json_str", json_str)

//json string to js object
var js_obj = JSON.parse(json_str)
console.log("js_obj", js_obj)

//js object to json string
var json_str2 = JSON.stringify(js_obj)
console.log("json_str2", json_str2)