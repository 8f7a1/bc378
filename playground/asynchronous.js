let text = "Hello"

call_db().then(function (a) {
    text = text + a
    console.log(1, text)
})


async function main(){
    console.log("run main")

    let text2 = "Hello"
    text2 = text2 + (await call_db())
    console.log(2, text2)
}
main()
console.log('end')









function call_db() {
    return new Promise((resolve, reject) => {
        setTimeout(function () {
            resolve("World")
        }, 5000)
    })
}