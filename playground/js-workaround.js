const a = 10 //constant variable
var b = 10 //variable
let c = 10 //variable

console.log(a) //print value of "a" to termimal

//declare function without argument and no return
function a_func(){
    //coding
}

//declare function with argument but without return
function b_func(arg1, arg2, arg3){
    //coding
}

//declare function with argument and return
function c_func(arg1, arg2){
    //coding
    return arg1 + arg2
}

//declare arrow function
const d_func = (/* arguments */) => {
    //coding
    console.log(this)
}

const e_func = function(/* arguments */){
    //coding
    console.log(this)
}


//call all function
a_func()
b_func()
c_func()
d_func()
e_func()
