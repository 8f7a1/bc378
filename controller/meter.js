const { getDbClient } = require('../db')

async function getMeters(req, res) {
    const meter = getDbClient().db("dlms").collection("meter")
    const result = await meter.find({}).toArray()
    console.log(result)
    res.json(result)
}

async function createMeter(req, res) {
    const meter = getDbClient().db("dlms").collection("meter")
    const data = req.body
    await meter.insertOne(data)
    res.json({ ok: true })
}

async function createMeters(req, res) {
    const meter = getDbClient().db("dlms").collection("meter")
    const data = req.body
    await meter.insertMany(data)
    res.json({ ok: true })
}

module.exports = {
    getMeters,
    createMeter,
    createMeters
}